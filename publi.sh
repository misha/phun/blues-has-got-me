#!/bin/bash

EXPORTDIR="public"

#############
# REVEAL.JS #
#############

ARCHIVE="master.zip"
REVEALDIR="reveal.js-master"
wget https://github.com/hakimel/reveal.js/archive/${ARCHIVE}
unzip ${ARCHIVE}
cp -R ${REVEALDIR}/dist ${EXPORTDIR}/
cp -R ${REVEALDIR}/plugin ${EXPORTDIR}/

# cleanup
rm -rf ${ARCHIVE} ${REVEALDIR}
